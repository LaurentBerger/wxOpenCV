var searchData=
[
  ['nbcouleurpalette',['nbCouleurPalette',['../class_fenetre_principale.html#a3a833ceb0842ee9aff50e03d53befd92',1,'FenetrePrincipale']]],
  ['nbgraines',['nbGraines',['../class_fenetre_histogramme.html#abefcfc0ae5caa30040a2c3258b4242a4',1,'FenetreHistogramme']]],
  ['nivbiais',['nivBiais',['../class_fenetre_principale.html#af9e5056c6096b67eb05fe55a9b486897',1,'FenetrePrincipale']]],
  ['nomdudocument',['nomDuDocument',['../class_fenetre_principale.html#a18fc11e4ecb430e71ebdf5baf60ae57e',1,'FenetrePrincipale']]],
  ['nomimagebiais',['nomImageBiais',['../class_fenetre_principale.html#a8944393fd5a8d2ff0866bef08ddca63b',1,'FenetrePrincipale']]],
  ['nomimagequadrique',['nomImageQuadrique',['../class_fenetre_principale.html#a598b5d707ebbc14a86928ca4d17bb3fb',1,'FenetrePrincipale']]],
  ['nomimagetache',['nomImageTache',['../class_fenetre_principale.html#a2e02b775729519e12395edf7b2b8a4af',1,'FenetrePrincipale']]],
  ['nomoperationpre',['nomOperationPre',['../class_fenetre_principale.html#a737a92afb37ce613dac5207dc2a2cc02',1,'FenetrePrincipale']]]
];
